package com.retrofit.interfaces;

import com.retrofit.entity.Comment;
import com.retrofit.entity.Post;
import com.retrofit.entity.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface WebServices {
    @GET("/users")
    void getUser(Callback<List<User>> callback);

    @GET("/posts/{param}")
    void getNPost(@Path("param") String param, Callback<Post> callback);

    @GET("/posts/{param}/comments")
    void getPostComments(@Path("param") String param, Callback<List<Comment>> callback);

    @GET("/posts")
    void getPostsByUser(@Query("userId") int param, Callback<List<Post>> callback);

}


