package com.retrofit.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.retrofit.R;
import com.retrofit.entity.User;
import com.retrofit.interfaces.WebServices;
import com.retrofit.util.GlobalClass;
import com.retrofit.util.ListViewAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity {


    WebServices webServices;
    ListView listView;
    ArrayList<User> userList;
    ListViewAdapter listAdapter;
    TextView name, uname, email, phone, website, address, company;
    int selectedItem = 0;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GlobalClass global = (GlobalClass) getApplicationContext();
        webServices = global.getInterface();
        listView = (ListView) findViewById(R.id.userListView);
        name = (TextView) findViewById(R.id.name);
        uname = (TextView) findViewById(R.id.uname);
        email = (TextView) findViewById(R.id.email);
        phone = (TextView) findViewById(R.id.phone);
        website = (TextView) findViewById(R.id.website);
        address = (TextView) findViewById(R.id.address);
        company = (TextView) findViewById(R.id.company);
        userList = new ArrayList<>();
        listAdapter = new ListViewAdapter(this, userList);
        listView.setAdapter(listAdapter);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait..");
        dialog.setTitle("Loading Data");
        dialog.show();
        getUsers();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = userList.get(position);
                selectedItem = position;
                setFields(user);
            }
        });
    }

    public void getUsers() {
        try {
            webServices.getUser(new Callback<List<User>>() {
                @Override
                public void success(List<User> users, Response response) {
                    User user;
                    for (int i = 0; i < users.size(); i++) {
                        user = users.get(i);
                        userList.add(user);
                    }
                    setFields(users.get(0));
                    listAdapter.notifyDataSetChanged();
                    dialog.dismiss();
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } catch (NullPointerException e) {
            e.fillInStackTrace();
        }

    }

//    public void getNPost(View view) {
//
//        try {
//            String param = ((EditText) findViewById(R.id.parameter)).getText().toString();
//            webServices.getNPost(param, new Callback<Post>() {
//                @Override
//                public void success(Post post, Response response) {
//                    String text = "";
//                    data.setText("");
//                    text += " UserId :" + post.getUserId() + "\n ID :"+ post.getId() + "\n Body : " + post.getBody() + "\n Title :" + post.getTitle() + "\n";
//                    data.setText(text);
//                }
//
//                @Override
//                public void failure(RetrofitError retrofitError) {
//                    Toast.makeText(MainActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
//                }
//            });
//        } catch (NullPointerException e) {
//            e.fillInStackTrace();
//        }
//
//    }

//    public void getPostComments(View view) {
//
//            String param = ((EditText) findViewById(R.id.parameter)).getText().toString();
//            webServices.getPostComments(param, new Callback<List<Comment>>() {
//
//                @Override
//                public void success(List<Comment> comments, Response response) {
//                    String text = "";
//                    data.setText("");
//                    Comment comment;
//                    try{
//                    for (int i = 0; i < comments.size(); i++) {
//                        comment = comments.get(i);
//                        text += " CommentID :" + comment.getId() + "\n Name :" + comment.getName() + "\n Body :" + comment.getBody()
//                                + "\n Email :" + comment.getEmail() + "\n";
//                    }
//                    data.setText(text);
//                    }catch(NullPointerException e) {
//                        Toast.makeText(MainActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void failure(RetrofitError retrofitError) {
//                    Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
//                }
//            });
//        }

    void setFields(User user) {
        name.setText(user.getName());
        uname.setText(user.getUsername());
        email.setText(user.getEmail());
        phone.setText(user.getPhone());
        website.setText(user.getWebsite());
        address.setText(user.address.street + " " + user.address.city);
        company.setText(user.company.name);
    }

    public void viewPosts(View view){
        User user = userList.get(selectedItem);
        Intent intent = new Intent(MainActivity.this,UserPosts.class);
        intent.putExtra("id",user.getId());
        startActivity(intent);
    }




}
