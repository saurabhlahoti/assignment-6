package com.retrofit.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.retrofit.R;
import com.retrofit.entity.Post;
import com.retrofit.interfaces.WebServices;
import com.retrofit.util.GlobalClass;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class UserPosts extends Activity {

    WebServices webServices;
    TextView textPosts;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_posts);
        webServices = ((GlobalClass) getApplicationContext()).getInterface();
        textPosts = (TextView) findViewById(R.id.posts);
        int uId = getIntent().getIntExtra("id",-1);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait..");
        dialog.setTitle("Loading Data");
        dialog.show();
        getPostsByUser(uId);
    }

    public void getPostsByUser(int userId) {
        try {
            webServices.getPostsByUser(userId, new Callback<List<Post>>() {
                @Override
                public void success(List<Post> posts, Response response) {
                    try {
                        Post post;
                        String text = "";
                        for (int i = 0; i < posts.size(); i++) {
                            post = posts.get(i);
                            text+="Title : "+ post.getTitle() + "\n\n Body : " + post.getBody() + "\n\n\n";
                        }
                        textPosts.setText(text);

                    } catch (NullPointerException e) {
                        Toast.makeText(UserPosts.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    Toast.makeText(UserPosts.this, "Failed", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } catch (NullPointerException e) {
            e.fillInStackTrace();
        }

    }


}
