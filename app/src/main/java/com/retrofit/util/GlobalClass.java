package com.retrofit.util;


import android.app.Application;

import com.retrofit.interfaces.WebServices;

import retrofit.RestAdapter;

public class GlobalClass extends Application {

    final String domain = "http://jsonplaceholder.typicode.com";
    WebServices webServices;

    public WebServices getInterface() {
        webServices = new RestAdapter.Builder()
                .setEndpoint(domain).build().create(WebServices.class);
        return webServices;
    }
}
