package com.retrofit.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.retrofit.R;
import com.retrofit.entity.User;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {

    ArrayList<User> userList;
    LayoutInflater inflater;
    Context context;


    public ListViewAdapter(Context context, ArrayList<User> list) {
        this.userList = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public User getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView name;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.user_list_structure, parent, false);
            name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(name);
        } else {
            name = (TextView) convertView.getTag();
        }

        User student = getItem(position);
        name.setText(student.getName());
        return convertView;
    }


    public ArrayList<User> getStudentList() {
        return userList;
    }


    public void setStudentList(ArrayList<User> studentList) {
        this.userList = studentList;
    }

}

